package com.sudomakmur.games

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val DATA_COMPUTER = "DATA_COMPUTER"

    var textResult : AppCompatTextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textResult = findViewById(R.id.textResult)

        textResult?.text = "LOSE"
        textResult?.background = getDrawable(R.drawable.bg_lose_game)
        textResult?.visibility = View.GONE


        btnRock.setOnClickListener {
            imgDisplayPlayer.setImageResource(R.drawable.ic_scissors_hand)
        }

    }


    fun randomComputer() {

    }

}